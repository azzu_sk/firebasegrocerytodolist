//
//  GroceryTableViewCell.swift
//  FireTest
//
//  Created by Nan on 09/04/19.
//  Copyright © 2019 PyPrac. All rights reserved.
//

import UIKit

class GroceryTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblGroceryName: UILabel!
    @IBOutlet weak var lblGroceryEmail: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}


