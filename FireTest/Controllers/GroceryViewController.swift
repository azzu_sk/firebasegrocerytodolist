//
//  GroceryViewController.swift
//  FireTest
//
//  Created by Nan on 09/04/19.
//  Copyright © 2019 PyPrac. All rights reserved.
//

import UIKit
import Firebase


class GroceryViewController: UIViewController {

    @IBOutlet weak var tblView: UITableView!
    
    let ref = Database.database().reference(withPath: "grocery-items")
    var items = [NSDictionary]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        orderList()
    }
    @IBAction func actionAdd(_ sender: Any) {
        addGrocery()
    }
    
}

extension GroceryViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GroceryTableViewCell") as! GroceryTableViewCell
        let item = items[indexPath.row]
        cell.lblItemName.text = (item.value(forKey: "item") as! String)
        cell.lblUserId.text = (item.value(forKey: "user") as! String)
        if((item.value(forKey: "complete")) as! Bool){
            cell.lblItemName.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
            cell.lblUserId.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
            cell.accessoryType = .checkmark
            cell.tag = 1
        }else{
            cell.lblItemName.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            cell.lblUserId.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            cell.accessoryType = .none
            cell.tag = 0
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        let key = items[indexPath.row].value(forKey: "item") as! String
        if(cell?.tag == 0){
            ref.child(key.lowercased()).updateChildValues([
                "complete": true
                ])
        }else{
            ref.child(key.lowercased()).updateChildValues([
                "complete": false
                ])
        }
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65.0
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        let key = items[indexPath.row].value(forKey: "item") as! String
        if editingStyle == .delete {
            ref.child(key.lowercased()).removeValue()
        }
    }
    
    func addGrocery() {
        let alertVc = UIAlertController(title: "Add Grocery", message: "Input name of grocery", preferredStyle: .alert)
        let alertAc = UIAlertAction(title: "Add", style: .default) { (action) in
            let txtGrocery = alertVc.textFields?.first
            let groceryDetails = ["item":txtGrocery?.text as Any,
                          "user": "test@123.com",
                          "complete": false
                ] as [String : Any]
            let groceryItemRef = self.ref.child((txtGrocery?.text!.lowercased())!)
            groceryItemRef.setValue(groceryDetails)
        }
        alertVc.addTextField { (txtField:UITextField) in
            txtField.placeholder = "Enter name of grocery"
            txtField.borderStyle = .none
        }
        let cancelAc = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        alertVc.addAction(cancelAc)
        alertVc.addAction(alertAc)
        self.present(alertVc, animated: true, completion: nil)
    }
    
    func fetchData() {
        ref.observe(.value, with: { snapshot in
            var newItems = [NSDictionary]()
            for child in snapshot.children {
                if let snapshot = child as? DataSnapshot{
                    let snapshotValue = snapshot.value as! [String: AnyObject]
                    newItems.append(snapshotValue as NSDictionary)
                }
            }
            self.items = newItems
            self.tblView.reloadData()
        })
    }
    
    func orderList() {
        ref.queryOrdered(byChild: "complete").observe(.value, with: { snapshot in
            var newItems = [NSDictionary]()
            for child in snapshot.children {
                if let snapshot = child as? DataSnapshot{
                    let snapshotValue = snapshot.value as! [String: AnyObject]
                    newItems.append(snapshotValue as NSDictionary)
                }
            }
            self.items = newItems
            self.tblView.reloadData()
        })
    }
}
