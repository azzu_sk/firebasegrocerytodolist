//
//  ViewController.swift
//  FireTest
//
//  Created by Nan on 08/04/19.
//  Copyright © 2019 PyPrac. All rights reserved.
//

import UIKit
import TweeTextField

class ViewController: UIViewController {

    @IBOutlet weak var txtEmail: TweeAttributedTextField!
    @IBOutlet weak var txtPassword: TweeAttributedTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
//        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    @IBAction func actionLogIn(_ sender: Any) {
        self.performSegue(withIdentifier: "GroceryViewController", sender: self)
    }
    @IBAction func actionSignUp(_ sender: Any) {
        
    }
    @IBAction func actionGoogleSignIn(_ sender: Any) {
        
    }
}

